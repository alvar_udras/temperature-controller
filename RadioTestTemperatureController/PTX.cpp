﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

#include <RF24_config.h>
#include <RNF24Commands.h>
#include <printf.h>
#include <nRF24L01.h>
#include <SPI.h>
#include <LiquidCrystal.h>
#include "RF24.h"
//Beginning of Auto generated function prototypes by Atmel Studio
void setup();
void readButton ();
void radioComm( uint8_t command );
void loop();
//End of Auto generated function prototypes by Atmel Studio


RF24 radio(A4,A3);
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
const int buttonPin = A2;
const int middleButtonTreshold = 990;
const int rightButtonTreshold = 520;

enum SelectedAction
{
	SwitchRelayOn,
	SwitchRelayOff,
	AskRelayState,
	RadioAvailable
};

SelectedAction currentRequest;

void setup()
{
	lcd.begin(16,2);	

	Serial.begin(57600);
	printf_begin();
	printf_P(PSTR("Setting up radio...\r\n" ) );
	radio.begin();
	radio.setPayloadSize(1);
	radio.openWritingPipe(pipeRelay);
	radio.openReadingPipe(1, pipeController );
	radio.powerUp();
	radio.printDetails();
	radio.startListening();

	currentRequest = AskRelayState;
}

void readButton ()
{
	int  buttonReading = analogRead( buttonPin );
	bool leftButtonPressed = false;
	bool middleButtonPressed = false;
	//printf_P(PSTR( "noise level %d\r\n" ), buttonReading );

	if ( buttonReading > 10 )
	{	
		delay ( 10 );
		buttonReading = analogRead ( buttonPin );
		printf_P(PSTR( "button down detected value %d\r\n" ), buttonReading );
		while ( analogRead ( buttonPin ) > 10 )
		{}
		if ( buttonReading > rightButtonTreshold && buttonReading < middleButtonTreshold )
			middleButtonPressed = true;
		else if ( buttonReading > middleButtonTreshold )
			leftButtonPressed = true;
		if ( middleButtonPressed )
		{
			lcd.clear();
			lcd.setCursor( 0, 0 );
			if ( currentRequest == AskRelayState )
			{
				currentRequest = SwitchRelayOn;
				lcd.print("Switch Relay on");	
			}
			else if ( currentRequest == SwitchRelayOn )
			{
				currentRequest = SwitchRelayOff;
				lcd.print("Switch Relay off");
			}
			else if ( currentRequest == SwitchRelayOff )
			{
				currentRequest = RadioAvailable;
				lcd.print("Ask if radio available");
			}
			else if ( currentRequest == RadioAvailable )
			{
				currentRequest = AskRelayState;
				lcd.print("Ask Relay State");
			}
		}
		else if ( leftButtonPressed )
		{
			lcd.clear();
			lcd.setCursor( 0, 0 );
			if ( currentRequest == RadioAvailable )
			{
				currentRequest = SwitchRelayOff;
				lcd.print("Switch Relay off");	
			}
			else if ( currentRequest == SwitchRelayOff )
			{
				currentRequest = SwitchRelayOn;
				lcd.print("Switch Relay on");
			}
			else if ( currentRequest == SwitchRelayOn )
			{
				currentRequest = AskRelayState;
				lcd.print("Ask Relay State");
			}
			else if ( currentRequest == AskRelayState )
			{
				currentRequest = RadioAvailable;
				lcd.print("Ask if radio available");
			}
		}
		else
		{
			if ( currentRequest == AskRelayState )
				radioComm( REQUEST_STATUS );
			else if ( currentRequest == RadioAvailable )
				radioComm (  IS_AVAILABLE );
			else if ( currentRequest == SwitchRelayOn )
				radioComm( SWITCH_RELAY_ON );
			else if ( currentRequest == SwitchRelayOff )
				radioComm( SWITCH_RELAY_OFF );
		}
	}
}

void radioComm( uint8_t command )
{
	radio.stopListening();
	printf_P(PSTR("Writing command" ) );
	 
	if ( command == SWITCH_RELAY_ON )
		printf_P(PSTR("SWITCH_RELAY_ON" ) );
	if ( command == SWITCH_RELAY_OFF )
		printf_P(PSTR("SWITCH_RELAY_OFF" ) );
	if ( command == IS_AVAILABLE )
		printf_P(PSTR("IS_AVAILABLE" ) );
	if ( command == REQUEST_STATUS )
		printf_P(PSTR("REQUEST_STATUS" ) );

	unsigned long time = micros();
	uint8_t reply;

	if ( radio.write( &command, 1 ) )
	{
		printf_P(PSTR("write of command succeeded\n\r" ));
		radio.startListening();		
		while(!radio.available() && micros()-time < 1000000){}
		if(!radio.available())
		{
			printf_P(PSTR("Got blank response. round-trip delay: %lu microseconds\n\r"), micros()-time);
		}
		else
		{
			while( radio.available() )
			{
				radio.read( &reply, 1 );
				if ( reply == YES )
					printf_P(PSTR("Got response %d, which means YES round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else if ( reply == NO )
					printf_P(PSTR("Got response %d, which means NO round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else if ( reply == REQUEST_FAILED )
					printf_P(PSTR("Got response %d, which means REQUEST_FAILED round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else if ( reply == REQUEST_SUCCEEDED )
					printf_P(PSTR("Got response %d, which means REQUEST_SUCCEEDED round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else if ( reply == RELAY_ON )
					printf_P(PSTR("Got response %d, which means RELAY_ON round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else if ( reply == RELAY_OFF )
					printf_P(PSTR("Got response %d, which means RELAY_OFF round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
				else
					printf_P(PSTR("Got unknown response %d, round-trip delay: %lu microseconds\n\r"), reply,micros()-time);
			}
		}
	}
	else
	{
		printf_P(PSTR("write failed\n" ) );
	}
	radio.startListening();
}

void loop()
{
	readButton();
}
