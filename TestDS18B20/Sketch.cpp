﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

#include <LiquidCrystal.h>
#include <DS18B20.h>
#include <printf.h>
//Beginning of Auto generated function prototypes by Atmel Studio
void setup();
void loop();
//End of Auto generated function prototypes by Atmel Studio


const int ds18b20Pin = 8;
const int ds18b20PowerPin = 9;
DS18B20 ds18b20( ds18b20Pin, ds18b20PowerPin );
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);

const int buttonPin = A2;
const int middleButtonTreshold = 990;
const int rightButtonTreshold = 520;

const int NOT_USED = 0xFF;

class PowerSwitcher {
	public:
	PowerSwitcher( uint8_t pinNum ) : m_pinNum (pinNum) {
		if ( m_pinNum != NOT_USED )
		digitalWrite(m_pinNum, HIGH);
	}
	~PowerSwitcher() {
		if ( m_pinNum != NOT_USED )
		digitalWrite(m_pinNum, LOW);
	}
	private:
	uint8_t m_pinNum;
};

enum Operation { CheckTemperature
			   , CheckIfParasite
			   , ReadMemory
			   , ChangeResolution
			   , CheckResolution
			   , ResolutionTo9Bit
			   , ResolutionTo10Bit
			   , ResolutionTo11Bit
			   , ResolutionTo12Bit };
enum MenuLevel { MainMenu, ResolutionMenu };
enum ButtonPressed { LeftButton, MiddleButton, RightButton, NotPressed };


void setup()
{
	Serial.begin(57600);
	printf_begin();	
	lcd.clear();
	lcd.setCursor( 0, 0 );
	lcd.print( "Check temperature" );
}

void handleMainMenu( ButtonPressed buttonPressed );
void handleSetResolutionMenu( ButtonPressed buttonPressed );

MenuLevel menuLevel = MainMenu;

void loop()
{
	int  buttonReading = analogRead( buttonPin );
	ButtonPressed buttonPressed = LeftButton;

	if ( buttonReading > 0 )
		printf_P(PSTR( "button reading %d\r\n" ), buttonReading );


	if ( buttonReading > 10 )
	{	
		lcd.clear();
		lcd.setCursor( 0, 0 );
		delay ( 10 );
		buttonReading = analogRead ( buttonPin );
		printf_P(PSTR( "button down detected value %d\r\n" ), buttonReading );
		while ( analogRead ( buttonPin ) > 10 )
		{}
		if ( buttonReading > rightButtonTreshold && buttonReading < middleButtonTreshold )
			buttonPressed = MiddleButton;
		else if ( buttonReading > middleButtonTreshold )
			buttonPressed = LeftButton;
		else
			buttonPressed = RightButton;
	} else {
		buttonPressed = NotPressed;
	}

	if ( menuLevel == MainMenu ) {
		handleMainMenu( buttonPressed );
	} else if ( menuLevel == ResolutionMenu ) {
		handleSetResolutionMenu( buttonPressed );
	}
}

Operation currentOperation  = CheckTemperature;

void handleMainMenu( ButtonPressed buttonPressed ) {
	if ( buttonPressed == MiddleButton )
	{
		PowerSwitcher ps(ds18b20PowerPin);
		ds18b20.init();
		if ( currentOperation == CheckTemperature ) {
			
			printf_P(PSTR( "middle button pressed\r\n" ));
			DS18B20::State state = ds18b20.getState();
			if ( state == DS18B20::Ready )
			{
				printf_P(PSTR( "State ready, requesting temperature convert\r\n" ));
				DS18B20::State state = ds18b20.requestConvert();
			}
			bool doneReading = false;
			while ( !doneReading )
			{
				state = ds18b20.getState();
				if ( state == DS18B20::ConversionDone )
				{
					float temperature = ds18b20.readTemperature();
					printf_P(PSTR("Temerature reading: \r\n") ); Serial.println( temperature );
					lcd.print("Temp ");
					lcd.print(temperature);
					doneReading = true;
				}
				else
				{
					printf_P(PSTR( "Not ready with conversion...\r\n" ));
				}
				delay ( 10 );
			}
		}

		if ( currentOperation == CheckIfParasite ) {
			if ( ds18b20.isInParasitePowerMode() ) {
				lcd.print ( "In Parasitic mode" );
			} else {
				lcd.print ( "In normal mode" );
			}
		}
		
		if ( currentOperation == ReadMemory ) {
			uint8_t buffer[9];
			ds18b20.readScratchPad( buffer );
			for( int i = 0; i < 9; i++) {		
				printf("%x ", buffer[i]);
				lcd.print( buffer[i] );
			}
			printf( PSTR( "\r\n" ) );
		}

		if ( currentOperation == CheckResolution ) {
			lcd.print( "resolution " );			
			DS18B20::Resolution resolution = ds18b20.getResolution();
			if ( resolution == DS18B20::Bit9 ) {
				lcd.print( "9 Bit" );
			} else if ( resolution == DS18B20::Bit10 ) {
				lcd.print( "10 Bit" );
			} else if ( resolution == DS18B20::Bit11 ) {
				lcd.print( "11 Bit" );
			} else if ( resolution == DS18B20::Bit12 ) {
				lcd.print( "12 Bit" );
			}
		}

		if ( currentOperation == ChangeResolution ) {
			lcd.print( "Resolution to 9 Bit" );
			currentOperation = ResolutionTo9Bit;
			menuLevel = ResolutionMenu;
		}
	}

	// read power supply mode of ds18b20
	else if (  buttonPressed == LeftButton  )
	{
		if ( currentOperation == CheckTemperature ) {
			currentOperation = CheckIfParasite;
			lcd.print ( "Check if in parasite mode" );
		} else if ( currentOperation == CheckIfParasite ) {
			currentOperation = ReadMemory;
			lcd.print ( "Read scratchpad memory" );
		} else if ( currentOperation == ReadMemory ) {			
			currentOperation = ChangeResolution;
			lcd.print( "Change resolution" );
		} else if ( currentOperation == ChangeResolution ) {
			currentOperation = CheckResolution;
			lcd.print( "Check Resolution" );
		} else if ( currentOperation == CheckResolution ) {
			currentOperation = CheckTemperature;
			lcd.print( "Check Temperature" );
		}
	}
	else if (  buttonPressed == RightButton  )
	{
		if ( ds18b20.reset() == 1 )
		{
			lcd.print ( "ds18b20 is responding" );
		}
		else
		{
			lcd.print ( "ds18b20 is not responding" );
		}
	}
}

void handleSetResolutionMenu( ButtonPressed buttonPressed ) {
	if ( buttonPressed == MiddleButton )
	{
		if ( currentOperation == ResolutionTo9Bit ) {
			lcd.print ( "Setting resolution to 9 Bit" );
			ds18b20.setResolution( DS18B20::Bit9 );
		} else if ( currentOperation == ResolutionTo10Bit ) {
			lcd.print ( "Setting resolution to 10 Bit" );
			ds18b20.setResolution( DS18B20::Bit10 );
		} else if ( currentOperation == ResolutionTo11Bit ) {
			lcd.print ( "Setting resolution to 11 Bit" );
			ds18b20.setResolution( DS18B20::Bit11 );
		} else if ( currentOperation == ResolutionTo12Bit ) {
			lcd.print ( "Setting resolution to 12 Bit" );
			ds18b20.setResolution( DS18B20::Bit12 );
		}
	}

	// read power supply mode of ds18b20
	else if (  buttonPressed == LeftButton  )
	{
		if ( currentOperation == ResolutionTo9Bit ) {
			currentOperation = ResolutionTo10Bit;
			lcd.print ( "Resolution to 10 Bit" );
		} else if ( currentOperation == ResolutionTo10Bit ) {
			currentOperation = ResolutionTo11Bit;
			lcd.print ( "Resolution to 11 Bit" );
		} else if ( currentOperation == ResolutionTo11Bit ) {			
			currentOperation = ResolutionTo12Bit;
			lcd.print( "Resolution to 12 Bit" );
		} else if ( currentOperation == ResolutionTo12Bit ) {
			currentOperation = ResolutionTo9Bit;
			lcd.print( "Resolution to 9 Bit" );
		}
	}
	else if (  buttonPressed == RightButton  )
	{
		lcd.print( "Check temperature" );
		currentOperation = CheckTemperature;
		menuLevel = MainMenu;
	}
}