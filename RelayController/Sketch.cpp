﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <RNF24Commands.h>
//Beginning of Auto generated function prototypes by Atmel Studio
void setup();
int sendResponse( uint8_t resp );
void loop();
//End of Auto generated function prototypes by Atmel Studio


RF24 radio(4,5);

const int ledPin = 7;     // the number of the pushbutton pin

uint8_t inputData[1];

// Single radio pipe address for the 2 nodes to communicate.


void setup() {

	Serial.begin(57600);
	printf_begin();  
	// initialize the pushbutton pin as an input:
	pinMode(ledPin, OUTPUT);
	digitalWrite ( ledPin, HIGH );
  
	radio.begin();
	radio.setAutoAck(1);
	radio.setRetries(5,15);
	radio.setPayloadSize(1);
	radio.openWritingPipe(pipeController);
	radio.openReadingPipe(1, pipeRelay );
	radio.startListening();
	radio.powerUp();
	radio.printDetails();
  
}

int sendResponse( uint8_t resp )
{
	int retryCount = 0;
	//while ( retryCount < 1000 && !radio.write( &resp, 1 ) )
	//{
	//	printf_P(PSTR( "writing of response failed\r\n" ) );		
	//}
	//return retryCount;
	return radio.write(&resp, 1);
}


void loop(){
  // read the state of the pushbutton value:
  // if there is data ready
	if ( radio.available() )
	{
		// First, stop listening so we can talk
		radio.stopListening();
		printf ( "inside radio available ... \n" );


		// Fetch the payload, and see if this was the last one.
		radio.read( inputData, 1 );	
			
		if ( inputData[0] == SWITCH_RELAY_ON )
		{
			printf("SWITCH_RELAY got request to switch relay ON\n");
			digitalWrite(ledPin, HIGH);
			if ( sendResponse( RELAY_ON ) )
				printf( PSTR("SWITCH_RELAY writing of response succeeded\r\n" ) );
		}
		else if ( inputData[0] == SWITCH_RELAY_OFF )
		{
			printf("SWITCH_RELAY got request to switch relay OFF\n");
			digitalWrite( ledPin, LOW );
			if ( sendResponse( RELAY_OFF ) )
				printf( PSTR("SWITCH_RELAY writing of response succeeded\r\n" ) );
		}
		else if ( inputData[0] == REQUEST_STATUS )
		{
			printf("REQUEST_STATUS got request for status\n");			
			if ( digitalRead( ledPin ) == HIGH )
			{					
				if ( sendResponse( RELAY_ON ) )
					printf( PSTR("REQUEST_STATUS writing of response succeeded\r\n" ) );
					
			}
			else
			{				
				if ( sendResponse(RELAY_OFF) )
					printf( PSTR("REQUEST_STATUS writing of response succeeded\r\n" ) );
			}					
		} else {
			printf ( "unknown data received ... %d\n ", inputData[0]);
		}

		while ( radio.available() )
		{
			radio.read( inputData, 1 );	
			printf ( "unknown data received ... %d\n ", inputData[0]);
		}

		// Now, resume listening so we catch the next packets.
		radio.startListening();
	}
}
