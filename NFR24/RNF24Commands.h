#pragma once

// responses

const uint8_t REQUEST_FAILED = 0x1;
const uint8_t REQUEST_SUCCEEDED = 0x2;
const uint8_t RELAY_ON = 0x4;
const uint8_t RELAY_OFF = 0x5;
const uint8_t YES = 0x6;
const uint8_t NO = 0x7;
const uint8_t UNKNOWN_REQUEST = 0x8;

// id-s

const uint64_t pipeRelay = 0xE8E8F0F0E1LL;
const uint64_t pipeController = 0xF0F0F0F0D2LL;

// commands

const uint8_t SWITCH_RELAY = 0x1;
const uint8_t SWITCH_RELAY_ON = 0x2;
const uint8_t SWITCH_RELAY_OFF = 0x3;
const uint8_t IS_AVAILABLE = 0x4;
const uint8_t REQUEST_STATUS = 0x5;