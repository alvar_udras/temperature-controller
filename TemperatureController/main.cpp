// Disable some warnings for the Arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wattributes"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wuninitialized"

#include <Arduino.h>
#include <printf/printf.h>
#include <LiquidCrystal.h>
#include "TemperatureManager.h"
#include "RelayManager.h"
#include "ButtonHandler.h"
#include "Timers.h"

/*
* Libraries
*/


// Restore original warnings configuration
#pragma GCC diagnostic pop

LiquidCrystal lcd(2, 3, 4, 5, 6, 7);

void setup()
{
	lcd.begin(16,2);
	lcd.print("on k�ll �ige fail");
	Serial.begin( 57600 );
	printf_begin();
	printf( "Starting controller...\r\n");
	TemperatureManager::setupDS18B20();
	RelayManager::setupRadio();	
}

void printStateOnScreen ()
{
	lcd.clear();
	if ( RelayManager::relayControllerAvailable == true )
	{
		lcd.setCursor( 0, 0 );
		lcd.print ( "Trg: " );
		lcd.print ( TemperatureManager::getTargetTemperature() );
		lcd.print ( " rly: " );
		if ( RelayManager::relayIsOn() )
			lcd.print ( "on" );
		else
			lcd.print( "off" );
		lcd.setCursor( 0, 1 );
		lcd.print( "Temp: " );
		lcd.print( TemperatureManager::readAmbientTemperature() );
	}
	else
	{
		lcd.setCursor( 0,0 );
		lcd.print( "relay not responding!" );
	}
}


/*
* Main code called on reset; the sketch harness
*/

int main(void)
{
	init();
	
	setup();
	
	for (;;) {
		handleButtonPresses();
		RelayManager::handleSwitchingOfRelay();
		printStateOnScreen();
		Timers::updateTimers();
		delay( 250 );
	}
	return 0;
}