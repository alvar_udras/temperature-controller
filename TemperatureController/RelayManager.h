#include "RF24.h"
#include <Arduino.h>


class RelayManager {
public:
	enum RelayState { RelayOn, RelayOff, Unknown };
	static void handleSwitchingOfRelay();
	static void setupRadio();
	static void switchRelayOverRadio( );
	static void requestSwitchRelayOff();
	static void requestSwitchRelayOn();
	static void requestRelayStateOverRadio();
	static void registerRequestRelayStateOverRadioOnTimer();
	static void registerSwitchRelayCallbackOnTimer();
	static bool relayIsOn();
//variables
	static bool relayControllerAvailable;
private:
	static RelayState actualRelayState;
	static RelayState requestedRelayState;
	static bool readOneByteResponse( uint8_t& response );
};