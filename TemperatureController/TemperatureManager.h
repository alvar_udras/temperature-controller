#ifndef ReadTemperature_h
#define ReadTemperature_h

#include <Arduino.h>
#include <Timer/Timer.h>

class TemperatureManager {
public:
	static float readAmbientTemperature ( );
	static int getTargetTemperature();
	static void increaseTargetTemperature();
	static void decreaseTargetTemperature();
	static void setupDS18B20();
	static void checkIfTemperatureReadingDone();

private:
	static float ambientTemperature;
	static float targetTemperature;
	static bool readingInProgress;
};
#endif