/* File automatically created by MariaMole */ 
#include "TemperatureManager.h"
#include "Timers.h"
#include "DS18B20.h"

DS18B20 ds18b20( 8, 9 );

float TemperatureManager::ambientTemperature = 20;
float TemperatureManager::targetTemperature = 20;
bool TemperatureManager::readingInProgress = false;

const int interval = 10000;

void TemperatureManager::checkIfTemperatureReadingDone()
{
    if ( ds18b20.getState() == DS18B20::ConversionDone )
	{
		ambientTemperature = ds18b20.readTemperature();
		readingInProgress = false;
	}
	else
	{
        Timers::temperatureTimer.after( interval, checkIfTemperatureReadingDone );
	}
}

float TemperatureManager::readAmbientTemperature ( )
{
	if ( !readingInProgress )
	{
        if ( ds18b20.requestConvert() != DS18B20::PendingConversion )
        {
            Serial.println( "readAmbientTemperature ( ) -> Requesting convert failed, returning...");
        }
		readingInProgress = true;
        Timers::temperatureTimer.after( interval, checkIfTemperatureReadingDone );
	}
	return ambientTemperature;
}

int TemperatureManager::getTargetTemperature()
{
	return targetTemperature;
}

void TemperatureManager::increaseTargetTemperature()
{
	targetTemperature++;
}

void TemperatureManager::decreaseTargetTemperature()
{
	targetTemperature--;
}


void TemperatureManager::setupDS18B20()
{
    printf("setupDS18B20()\r\n");
    ds18b20.init();
}
