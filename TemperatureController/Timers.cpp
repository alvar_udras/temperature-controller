#include "Timers.h"

int Timers::relayRequestStatusTimerId = -1;
int Timers::relaySwitchTimerId = -1;
int Timers::temperatureTimerId =-1;

Timer Timers::temperatureTimer;
Timer Timers::relayTimer;

void Timers::updateTimers()
{
    Timers::temperatureTimer.update();
    Timers::relayTimer.update();
}
