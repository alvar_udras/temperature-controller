#include "ButtonHandler.h"

const int buttonPin = A2;
const int downButtonTreshold = 700;

bool buttonPressed ( Direction direction )
{
	bool buttonDown = false;
	delay( 10 );
	int  buttonReading = analogRead( buttonPin );
	if ( buttonReading > 2 )
	{
		if ( buttonReading > downButtonTreshold && direction ==  Down )
			buttonDown = true;
		else if (  buttonReading < downButtonTreshold && direction == Up )
			buttonDown = true;
		Serial.print( "button down detected value " ); Serial.println( buttonReading );
	}
	while ( buttonDown )
	{
		delay( 10 );
		if ( analogRead ( buttonPin ) == 0 )
			return true;
	}
	return false;
}

void handleButtonPresses () 
{
	if ( buttonPressed( Up ) )
	{
		TemperatureManager::increaseTargetTemperature();
	}
	if ( buttonPressed ( Down ) )
	{
		TemperatureManager::decreaseTargetTemperature();
	}
}
