#ifndef TIMERS_H
#define TIMERS_H

#include <Timer/Timer.h>

class Timers
{
public:
    static Timer temperatureTimer;
    static int temperatureTimerId;
    static Timer relayTimer;
    static int relaySwitchTimerId;
	static int relayRequestStatusTimerId;
    static void updateTimers();
};

#endif
