#include <Arduino.h>
#include "TemperatureManager.h"

enum Direction { Up, Down };

bool buttonPressed ( Direction direction );

void handleButtonPresses ();
