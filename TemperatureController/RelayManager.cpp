#include "RelayManager.h"
#include "TemperatureManager.h"
#include "Timers.h"
#include <RNF24Commands.h>

// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipeOutput = 0xE8E8F0F0E1LL;
const uint64_t pipeInput = 0xF0F0F0F0D2LL;

RelayManager::RelayState RelayManager::actualRelayState = RelayManager::Unknown;
RelayManager::RelayState RelayManager::requestedRelayState = RelayManager::Unknown;

bool RelayManager::relayControllerAvailable = false;

RF24 radio(A4,A3);

int timerId = -1;

void RelayManager::setupRadio()
{
	printf ("Setting up radio...\r\n" );
	radio.begin();
	radio.setAutoAck(1);
	radio.setPayloadSize(1);
	radio.openWritingPipe(pipeRelay);
	radio.openReadingPipe(1, pipeController );
	radio.startListening();
	radio.powerUp();
	radio.printDetails();
	requestRelayStateOverRadio();
	if ( TemperatureManager::readAmbientTemperature() >= TemperatureManager::getTargetTemperature() &&  actualRelayState == RelayOn  )
	{	
		printf_P(PSTR( "switching relay off\r\n" ) );
		requestSwitchRelayOff( );
	}
	else if ( TemperatureManager::readAmbientTemperature() < TemperatureManager::getTargetTemperature() && actualRelayState == RelayOff  )
	{
		printf_P(PSTR( "switching relay on\r\n") );
		requestSwitchRelayOn( );
	}
	registerRequestRelayStateOverRadioOnTimer();
}

void RelayManager::registerSwitchRelayCallbackOnTimer()
{
	printf_P(PSTR("registerSwitchRelayCallbackOnTimer registering callback on timer\r\n" ) );
	if ( Timers::relaySwitchTimerId != -1 )
		Timers::relayTimer.stop( Timers::relaySwitchTimerId );
	Timers::relaySwitchTimerId = Timers::relayTimer.after( 5000, &RelayManager::switchRelayOverRadio );
}

bool RelayManager::readOneByteResponse(uint8_t& response) 
{
	response = 0;
	radio.startListening();

	printf_P(PSTR("requestRelayStateOverRadio waiting for response\r\n"));
	unsigned long time = micros();
	while ( !radio.available() && micros()-time < 1000000){}
		
	if ( !radio.available() )
	{
		printf_P(PSTR("requestRelayStateOverRadio no response after 1 second retries, returning\r\n")); 
		relayControllerAvailable = false;
		return false;
	}
	
	printf_P(PSTR("requestRelayStateOverRadio got state response\r\n") );		

	radio.stopListening();
	radio.read( &response, 1 );
	return true;
}

void RelayManager::requestRelayStateOverRadio() 
{
	radio.stopListening();
	bool ok = radio.write( &REQUEST_STATUS, 1 );
    if (ok)
	{
		relayControllerAvailable = true;
		printf_P(PSTR("requestRelayStateOverRadio writing to radio ok\r\n" ));
	}
	else
	{
		relayControllerAvailable = false;
		printf_P(PSTR("requestRelayStateOverRadio writing to radio failed\r\n" ) );
		return;
	}

	uint8_t response = 0;

	if ( !readOneByteResponse(response) )
	{
		return;
	}

	if ( response == RELAY_ON )
	{
		actualRelayState = RelayOn;
		printf_P(PSTR("requestRelayStateOverRadio radio was on\r\n") );
	}
	else if ( response == RELAY_OFF )
	{
		actualRelayState = RelayOff;
		printf_P(PSTR("requestRelayStateOverRadio radio was off\r\n"));
	}
	else
		printf_P(PSTR("requestRelayStateOverRadio: unknown response\r\n")  );
	radio.startListening();

	return;
}

void RelayManager::registerRequestRelayStateOverRadioOnTimer()
{
	Timers::relayTimer.every( 5000, requestRelayStateOverRadio );
}

void RelayManager::switchRelayOverRadio( )
{
	radio.stopListening();
	printf_P(PSTR("switchRelayOverRadio Button state changed\r\n") );
	uint8_t dataPacket[2]; 
	if ( requestedRelayState == RelayOn )
		dataPacket[0] = SWITCH_RELAY_ON;
	else
		dataPacket[0] = SWITCH_RELAY_OFF;
	bool ok = radio.write( dataPacket, 2 );

    if (ok)
	{
		relayControllerAvailable = true;
		printf_P(PSTR("switchRelayOverRadio write to radio success\r\n") );
	}
	else
	{
		relayControllerAvailable = false;
		printf_P(PSTR("switchRelayOverRadio write to radio failed\r\n") );
		radio.startListening();
		return;
	}

	uint8_t response;
	if ( !readOneByteResponse(response) )
		return;

	if ( response == RELAY_ON )
	{
		actualRelayState = RelayOn;
		printf_P(PSTR("switchRelayOverRadio radio was on\r\n") );
	}
	else if ( response == RELAY_OFF )
	{
		actualRelayState = RelayOff;
		printf_P(PSTR("switchRelayOverRadio radio was off\r\n"));
	}
	else
		printf_P(PSTR("switchRelayOverRadio: unknown response\r\n")  );
        
	radio.startListening();
	requestedRelayState = Unknown;
	return;
}

void RelayManager::requestSwitchRelayOff()
{
	requestedRelayState = RelayOff;
	RelayManager::registerSwitchRelayCallbackOnTimer();
}
void RelayManager::requestSwitchRelayOn()
{
	requestedRelayState = RelayOn;
	RelayManager::registerSwitchRelayCallbackOnTimer();
}

void RelayManager::handleSwitchingOfRelay()
{
	if ( TemperatureManager::readAmbientTemperature() >= TemperatureManager::getTargetTemperature() &&  actualRelayState == RelayOn && requestedRelayState != RelayOff )
	{	
		printf_P(PSTR( "switching relay off\r\n" ) );
		requestSwitchRelayOff( );
	}
	else if ( TemperatureManager::readAmbientTemperature() < TemperatureManager::getTargetTemperature() && actualRelayState == RelayOff && requestedRelayState != RelayOn )
	{
		printf_P(PSTR( "switching relay on\r\n") );
		requestSwitchRelayOn( );
	}
}

bool RelayManager::relayIsOn()
{
	return actualRelayState == RelayOn;
}