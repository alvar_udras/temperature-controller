﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

#include <SPI.h>
#include <RNF24Commands.h>
#include <RF24_config.h>
#include <RF24.h>
#include <printf.h>
#include <nRF24L01.h>
//Beginning of Auto generated function prototypes by Atmel Studio
void switchRelayOn();
void switchRelayOff();
uint8_t getRelayState();
void setup();
void loop();
//End of Auto generated function prototypes by Atmel Studio


RF24 radio( 4, 5 );

const int ledPin = 7;

void switchRelayOn()
{
	digitalWrite(ledPin, HIGH);
}

void switchRelayOff()
{
	digitalWrite(ledPin, LOW);
}

uint8_t getRelayState()
{
	if ( digitalRead( ledPin ) == HIGH )
		return RELAY_ON;
	else
		return RELAY_OFF;
}

void setup()
{
	Serial.begin(57600);
	printf_begin();
	printf_P(PSTR("Setting up radio...\r\n" ) );
	radio.begin();
	radio.setPayloadSize(1);
	radio.openWritingPipe(pipeController);
	radio.openReadingPipe(1, pipeRelay );
	radio.startListening();
	radio.powerUp();
	radio.printDetails();
	pinMode( ledPin, OUTPUT );
}

void loop()
{

	byte command;
	while( radio.available())
	{
		radio.stopListening();
		radio.read( &command, 1 );
		if ( command == IS_AVAILABLE )
		{
			printf_P(PSTR("Got request for IS_AVAILABLE\r\n" ) );
			if (!radio.write(&YES, 1 ) ) {
				printf_P(PSTR("Writing of response failed!!!\r\n" ) );
			}
			printf_P(PSTR("Wrote response YES\r\n" ) );
		}
		else if ( command == SWITCH_RELAY_ON )
		{
			printf_P(PSTR("Got request for SWITCH_RELAY_ON\r\n" ) );
			switchRelayOn();
			if ( !radio.write(&REQUEST_SUCCEEDED, 1 )) {
				printf_P(PSTR("Writing of response failed!!!\r\n" ) );
			}
			printf_P(PSTR("Wrote response REQUEST_SUCCEEDED\r\n" ) );
		}
		else if ( command == SWITCH_RELAY_OFF )
		{
			printf_P(PSTR("Got request for SWITCH_RELAY_OFF\r\n" ) );
			switchRelayOff();
			if ( !radio.write(&REQUEST_SUCCEEDED, 1 )) {
				printf_P(PSTR("Writing of response failed!!!\r\n" ) );
			}
			printf_P(PSTR("Wrote response REQUEST_SUCCEEDED\r\n" ) );
		}
		else if ( command == REQUEST_STATUS )
		{
			printf_P(PSTR("Got request for REQUEST_STATUS\r\n" ) );
			if ( getRelayState() == RELAY_ON )
			{
				if ( !radio.write(&RELAY_ON, 1 )) {
					printf_P(PSTR("Writing of response failed!!!\r\n" ) );
				}
				printf_P(PSTR("Wrote response RELAY_ON\r\n" ) );
			}
			else 
			{
				if ( !radio.write(&RELAY_OFF, 1 )) {
					printf_P(PSTR("Writing of response failed!!!\r\n" ) );
				}
				printf_P(PSTR("Wrote response RELAY_OFF\r\n" ) );
			}
		}
		else
		{
			if ( !radio.write(&UNKNOWN_REQUEST, 1 )) {
				printf_P(PSTR("Writing of response failed!!!\r\n" ) );
			}
		}
		radio.startListening();
	}
}
