//---------------------------------------------------------------------------
#include "DS18B20.h"

const int CMD_CONVERT = 0x44;
const int CMD_READ_SCRATCHPAD = 0xBE;
const int CMD_WRITE_SCRATCHPAD = 0x4E;
const int CMD_COPY_SCRATCHPAD = 0x48;
const int CMD_READ_POWER_SUPPLY = 0xB4;
const int CMD_SKIP_ROM = 0xCC;
const int NOT_USED = 0xFF;

class PowerSwitcher {
public:
	PowerSwitcher( uint8_t pinNum ) : m_pinNum (pinNum) {
		//if ( m_pinNum != NOT_USED )
			//digitalWrite(m_pinNum, HIGH);
	}
	~PowerSwitcher() {
		//if ( m_pinNum != NOT_USED )
			//digitalWrite(m_pinNum, LOW);
	}
private:
	uint8_t m_pinNum;
};

DS18B20::DS18B20( int pinNum ) : m_oneWireDevice( pinNum, 0 ), m_state( NotInitialized ), m_powerPin(NOT_USED)
{
    //printf("In DS18B20 constructor...\n\r");
}

DS18B20::DS18B20( int pinNum, int powerPin ) : m_oneWireDevice( pinNum, 0 ), m_state( NotInitialized ), m_powerPin(powerPin)
{
	//printf("In DS18B20 constructor...\n\r");
	pinMode(m_powerPin, OUTPUT);
	digitalWrite(m_powerPin, HIGH);
}

DS18B20::State DS18B20::init()
{	
	PowerSwitcher ps(m_powerPin);
    printf_P(PSTR("DS18B20::init()\r\n"));
    byte i;
    
    if ( !m_oneWireDevice.search(m_addr) )
    {
        printf(PSTR("No more m_addresses.\r\n"));
        m_oneWireDevice.reset_search();
        return m_state;
    }
	printf( PSTR( "DS18B20 Address is:\r\n" ));
    printf_P(PSTR("R="));
    for( i = 0; i < 8; i++)
    {		
        printf("%x ", m_addr[i]);
    }
	printf_P(PSTR("\r\n"));
    if ( OneWire::crc8( m_addr, 7) != m_addr[7])
    {
        printf_P(PSTR("CRC is not valid!\r\n"));
        return m_state;
    }
    if ( m_addr[0] != 0x28)
    {
        printf_P(PSTR("Device is not a DS18B20 family device.\r\n"));
        return m_state;
    }	
    
    m_state = Ready;

	if ( isInParasitePowerMode() ) {
		printf_P(PSTR("We are in parasitic power mode.\r\n"));
		m_oneWireDevice.setParasite( true );
	} else {
		printf_P(PSTR("We are in normal power mode.\r\n"));
	}

	uint8_t buffer[9];

	m_resolution = getResolution();
    
    return m_state;
}

DS18B20::State DS18B20::requestConvert()
{
	PowerSwitcher ps(m_powerPin);
	bool inParasite = isInParasitePowerMode();
    if ( m_state == NotInitialized || m_state == PendingConversion )
    {
        printf_P(PSTR("DS18B20 in invalid state for requesting conversion! returning ...\r\n "));
        return m_state;
    }
    m_oneWireDevice.reset();
    m_oneWireDevice.select(m_addr);
    m_oneWireDevice.write(CMD_CONVERT);
	if ( inParasite ) {
		delay( 1000 );
		m_oneWireDevice.depower();
	}
    m_state = PendingConversion;
    return m_state;
}

DS18B20::State DS18B20::getState()
{
	PowerSwitcher ps(m_powerPin);
    if ( m_state == PendingConversion )
    {
        if ( m_oneWireDevice.read_bit() )
        {
            printf_P( PSTR("conversion done\r\n" ) );
            m_state = ConversionDone;
        }
    }
    return m_state;
}
float DS18B20::readTemperature()
{
	PowerSwitcher ps(m_powerPin);
    if ( m_state != ConversionDone )
    {
        printf_P(PSTR("DS18B20 is in invalid state for reading temperature. returning....\r\n") );
    }
    m_oneWireDevice.reset();
    m_oneWireDevice.select(m_addr);
    m_oneWireDevice.write(CMD_READ_SCRATCHPAD);         // Read Scratchpad
    printf_P(PSTR("P="));
    printf_P(PSTR(" "));
    byte data[12];
    for ( int i = 0; i < 9; i++)
    {           // we need 9 bytes
        data[i] = m_oneWireDevice.read();
        //printf( "%x ", data[i]);
    }
	m_oneWireDevice.depower();
    //printf(" CRC=%x\r\n", OneWire::crc8( data, 8));
    
    int LowByte = data[0];
    int HighByte = data[1];
    int TReading = (HighByte << 8) + LowByte;
    int SignBit = TReading & 0x8000;  // test most sig bit
    if (SignBit) // negative
    {
        TReading = (TReading ^ 0xffff) + 1; // 2's comp
    }
    int Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

    int Whole = Tc_100 / 100;  // separate off the whole and fractional portions
    int Fract = Tc_100 % 100;
    float temperature = Whole + Fract/100.0;


    if (SignBit) // If its negative
    {
        printf("-");
    }
    printf_P(PSTR("%d"), Whole);
    printf(".");
    if (Fract < 10)
    {
        printf_P(PSTR("0"));
    }
    printf_P(PSTR("%d"), Fract);

    printf_P(PSTR("\r\n"));

    printf_P(PSTR("After converting to float " ) );Serial.print(temperature);Serial.println();
    
    m_state = Ready;
        
    return temperature;
}

boolean DS18B20::isInParasitePowerMode(void)
{
	PowerSwitcher ps(m_powerPin);
    if ( !m_oneWireDevice.reset() ) {
		printf("Device didnt asset its presenece!\r\n");
	}
    m_oneWireDevice.skip();
    m_oneWireDevice.write(CMD_READ_POWER_SUPPLY);
	delay(12);
    uint8_t res = m_oneWireDevice.read_bit();
	m_oneWireDevice.depower();
	if ( res != 1 ) {
		printf("we are in parasitic mode\r\n");
	} else {
		printf("we are in normal mode\r\n");
	}
	
    return res != 1;
}

void DS18B20::setResolution( Resolution targetResolution )
{
	PowerSwitcher ps(m_powerPin);
	m_oneWireDevice.reset();
	m_oneWireDevice.select(m_addr);
	m_oneWireDevice.write( CMD_WRITE_SCRATCHPAD );
	uint8_t data[3];
	data[0] = 0x4b;
	data[1] = 0x46;
	data[2] = targetResolution;

	m_oneWireDevice.write_bytes( data, 3 );

	uint8_t buffer[9];
	readScratchPad( buffer );

	printf( PSTR("Set resolution, result of read memory! \r\n"));

	for( int i = 0; i < 9; i++) {		
		printf("%x ", buffer[i]);
	}

	printf( PSTR("\r\n" ) );

	if ( buffer[4] != data[2] ) {
		printf( PSTR("written data does not match read data! \r\n"));
	}

	m_oneWireDevice.reset();
	m_oneWireDevice.select( m_addr );
	m_oneWireDevice.write( CMD_COPY_SCRATCHPAD );
	delay ( 12 );
	m_oneWireDevice.depower();

	m_resolution = targetResolution;
}

DS18B20::Resolution DS18B20::getResolution() {
	uint8_t buffer[9];
	readScratchPad( buffer );
	return (DS18B20::Resolution) buffer[4];
}

void DS18B20::readScratchPad( uint8_t buffer[] ) {
	PowerSwitcher ps(m_powerPin);
	m_oneWireDevice.reset();
	m_oneWireDevice.select( m_addr );
	m_oneWireDevice.write( CMD_READ_SCRATCHPAD );
	m_oneWireDevice.read_bytes( buffer, 9 );
	m_oneWireDevice.depower();
}

