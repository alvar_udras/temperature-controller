/* File automatically created by MariaMole */ 
#include <OneWire.h>

class DS18B20
{
public:
	enum State{ NotInitialized, Ready, PendingConversion, ConversionDone };
	enum Resolution{ Bit9 = 0x1f, Bit10 = 0x3f, Bit11 =  0x5f, Bit12 =  0x7f};
	DS18B20( int pinNum );
	DS18B20( int pinNum, int powerPin  );
	State requestConvert();
	State init();
	State getState();
	float readTemperature();
	boolean isInParasitePowerMode(void);
	uint8_t reset() {  return m_oneWireDevice.reset(); }
	void setResolution( Resolution targetResolution );
	Resolution getResolution();
	void readScratchPad( uint8_t buffer[] );
private:
	OneWire m_oneWireDevice;
	byte m_addr[8];
	State m_state;
	uint8_t m_parasite;
	Resolution m_resolution;
	uint8_t m_powerPin;
};
